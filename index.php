<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="PHP Practice - Data Types and Functions">
<meta name="keywords" content="PHP, practice, data type, function, howto">
<meta name="author" content="Mladen Reljić">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PHP Domaci</title>
</head>
<body>
    <h1>Data Types</h1>

    <h2>String</h2>
    <b>A string is a sequence of characters; i.e., "Hello World!"</b>
    <br>
    <u>$string="Hello World!"</u>
    <?php
        $string="Hello World!";
        echo "</br></br><span style='color:red'> " .$string. "</span>";
    ?>

    <h2>Integer</h2>
    <b>An integer data type is a non-decimal number - must have at least one digit, no decimal points, can be negative or positive, can be specified in three formats: decimal, hexadecimal, and octal; i.e., "1255"</b>
    <br>
    <u>$integer=1255</u>
    <?php
        $integer = 1255;
        echo "</br></br><span style='color:blue'> " .$integer. "</span>";
    ?>

    <h2>Float</h2>
    <b>A float (floating point number) is a number with a decimal point; i.e., "19.23"</b>
    <br>
    <u>$float = 19.23;</u>
    <?php
        $float = 19.23;
        echo "</br></br><span style='color:green'> " .$float. "</span>";
    ?>

    <h2>Boolean</h2>
    <b>A Boolean represents two possible states: <u>TRUE</u> or <u>FALSE</u>; i.e., $boolean=true.<b>
    <br>
    <u>$boolean = true;</u>
    <?php
        $boolean = true;
        echo "</br></br><span style='color:purple'> " .$boolean. "</span>";
    ?>

    <h2>Array</h2>
    <b>An array stores multiple values in one single variable; i.e., $drinks = array("juice", "beer", "liquor")</b>
    <br>
    <u>$drinks = array("juice", "beer", "liquor");</u>
    <br>
    <?php
        //definishi varijablu kao niz sa 3 pitja.
        $drinks = array("juice", "beer", "liquor");

        //funkcija "arej" koja loop-uje kroz svaku vrednost/key u $drinks nizu i ispisuje tu vrednost roza bojom.
        function arej ($arr) {
            foreach ($arr as $value) {
                echo "</br><span style='color:pink'> " .$value. "</span>"." ";
            }
        }
        //poziv funkcije
        arej($drinks);
    ?>

    <h1>Sum of Numbers Function</h1>
    <u>The numbers are: $numbers = array (6, 13, 24, 88, 202);</u>
    <br>
    <?php
        //ukljuchivanje funkcije iz function.php
        include 'function.php';
        $numbers = array (6, 13, 24, 88, 202);
        colournum($numbers, 'violet');
    ?>


    <h1>Min Number Function</h1>
    <u>The numbers are: $numberino = array(3, 6, 10, 45, 99, 202);</u>
    <?php 
        include 'minfunction.php';
        $numberino = array(3, 6, 10, 45, 99, 202);
        min_number($numberino, 'grey');
    ?>

    <h1>Max Number Function</h1>
    <u>The numbers are: $numberino = array(29, 55, 89, 123, 287, 1209);</u>
    <?php 
        include 'maxfunction.php';
        $numberino = array(29, 55, 89, 123, 287, 1209);
        max_number($numberino, 'green');
    ?>



</body>
</html>